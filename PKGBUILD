# This is an example PKGBUILD file. Use this as a start to creating your own,
# and remove these comments. For more information, see 'man PKGBUILD'.
# NOTE: Please fill out the license field for your package! If it is unknown,
# then please put 'unknown'.

# Maintainer: Future Linux Team <future_linux@163.com>
pkgname=(glibc glibc-locales)
pkgbase=glibc
pkgver=2.40
pkgrel=1
arch=('x86_64')
url="https://www.gnu.org/software/libc/"
license=('GPL-2.0-or-later' 'LGPL-2.1-or-later')
makedepends=('python')
options=('staticlibs' '!lto')
source=(https://ftp.gnu.org/gnu/${pkgbase}/${pkgbase}-${pkgver}.tar.xz
	nsswitch.conf
	ld.so.conf
	locale.gen.txt
	locale-gen
	sdt-config.h
	sdt.h)
sha256sums=(19a890175e9263d748f627993de6f4b1af9cd21e03f080e4bfb3a1fac10205a2
	c8ee7a9faf798caab178ec51afae4146f1efd8a716b7acedf28345b6c75f9697
	6c6813ccd4963758e4ca73f71ccde769e9028bbcfcc88285a8c8ce65d2af066c
	2a7dd6c906b6c54a68f48a21898664a32fdb136cbd9ff7bfd48f01d1aaa649ae
	8c5cc09018cbd65e8570430f872e118caa2644e311d6200b7d5f0cba837fbba4
	cdc234959c6fdb43f000d3bb7d1080b0103f4080f5e67bcfe8ae1aaf477812f0  
	774061aff612a377714a509918a9e0e0aafce708b87d2d7e06b1bd1f6542fe70)

prepare() {
	cd ${pkgbase}-${pkgver}

	install -vd ${pkgbase}-build
}

build() {
	cd ${pkgbase}-${pkgver}/${pkgbase}-build

	CFLAGS=${CFLAGS/-Wp,-D_FORTIFY_SOURCE=3/}

	echo "slibdir=/usr/lib64"    >> configparms
	echo "rtlddir=/usr/lib64"    >> configparms
	echo "sbindir=/usr/bin"      >> configparms
	echo "rootsbindir=/usr/sbin" >> configparms
	echo "complocaledir=/usr/lib/locale" >> configparms

	${BUILD_CONFIGURE}                      \
	       --with-headers=/usr/include      \
	       --enable-bind-now                \
	       --enable-fortify-source          \
	       --enable-kernel=4.19             \
	       --enable-stack-protector=strong  \
	       --disable-nscd                   \
	       --disable-profile                \
	       --disable-werror                 \
	       --enable-cet                     \
	       --disable-timezone-tools

	make -O

	# build info pages manually for reproducibility
	make info

	make -C ${srcdir}/${pkgbase}-${pkgver}/localedata \
		 objdir=${srcdir}/${pkgbase}-${pkgver}/${pkgbase}-build \
		 DESTDIR=${srcdir}/locales install-locale-files

}

package_glibc() {
	pkgdesc="GNU C Library"
	groups=('base')
	depends=('linux-api-headers' 'tzdata' 'filesystem')
	install=${pkgbase}.install
	backup=(etc/gai.conf
		etc/nsswitch.conf
		etc/ld.so.conf
		etc/locale.gen)

	cd ${pkgbase}-${pkgver}/${pkgbase}-build

	make DESTDIR=${pkgdir} install

	rm -f ${pkgdir}/etc/ld.so.cache

	install -dm755 ${pkgdir}/usr/lib/locale

	install -m644 ../posix/gai.conf ${pkgdir}/etc/gai.conf

	install -m755 ${srcdir}/locale-gen ${pkgdir}/usr/sbin

	# Create /etc/locale.gen
	install -m644 ${srcdir}/locale.gen.txt ${pkgdir}/etc/locale.gen
	sed -e '1,3d' -e 's|/| |g' -e 's|\\| |g' -e 's|^|#|g' \
		../localedata/SUPPORTED >> ${pkgdir}/etc/locale.gen

	# Add SUPPORTED file to pkg
	sed -e '1,3d' -e 's|/| |g' -e 's| \\||g' \
		../localedata/SUPPORTED > ${pkgdir}/usr/share/i18n/SUPPORTED

	# install C.UTF-8 so that it is always available
	# should be built into glibc eventually
	# https://sourceware.org/glibc/wiki/Proposals/C.UTF-8
	# https://bugs.archlinux.org/task/74864
	cp -r ${srcdir}/locales/usr/lib/locale/C.utf8 -t ${pkgdir}/usr/lib/locale
	sed -i '/#C\.UTF-8 /d' ${pkgdir}/etc/locale.gen

	install -vm644 ${srcdir}/nsswitch.conf ${pkgdir}/etc/
	install -vm644 ${srcdir}/ld.so.conf ${pkgdir}/etc/

	install -vdm755 ${pkgdir}/etc/ld.so.conf.d
	
	install -Dm644 ${srcdir}/sdt.h ${pkgdir}/usr/include/sys/sdt.h
	install -Dm644 ${srcdir}/sdt-config.h ${pkgdir}/usr/include/sys/sdt-config.h
}

package_glibc-locales() {
	pkgdesc="Pregenerated locales for GNU C Library"
	depends=("glibc=${pkgver}")

	cp -r locales/* -t ${pkgdir}
	rm -r ${pkgdir}/usr/lib/locale/C.utf8

	# deduplicate locale data
	hardlink -c ${pkgdir}/usr/lib/locale
}
